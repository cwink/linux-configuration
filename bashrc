# Source the bash utilities script.
source /usr/local/src/linux-configuration/utils.sh

if [ ${?} -ne 0 ]; then
  print_error 'Failed to source utilities script file.'

  exit 1
fi

# Perform a full update on the system.
# Returns code 1 for failure, code 0 for success.
full_update() {
  if [ "$(check_for_program 'apt')" == "TRUE" ]; then
    sudo apt -y update

    [[ ${?} -ne 0 ]] && return 1

    sudo apt -y upgrade

    [[ ${?} -ne 0 ]] && return 1

    sudo apt -y update

    [[ ${?} -ne 0 ]] && return 1

    sudo apt -y dist-upgrade

    [[ ${?} -ne 0 ]] && return 1

    sudo apt-get -y --purge autoremove

    [[ ${?} -ne 0 ]] && return 1
  elif [ "$(check_for_program 'yum')" == "TRUE" ]; then
    sudo yum update -y

    [[ ${?} -ne 0 ]] && return 1
  elif [ "$(check_for_program 'pacman')" == "TRUE" ]; then
    pacman -Syu

    [[ ${?} -ne 0 ]] && return 1
  else
    echo "Only package managers supported are 'apt', 'yum', and 'pacman'." >&2 1>&2

    return 1
  fi

  return 0
}

# Perform a full upgrade on the system.
# Returns code 1 for failure, code 0 for success.
full_upgrade() {
  full_update

  [[ ${?} -ne 0 ]] && return 1

  full_update

  [[ ${?} -ne 0 ]] && return 1

  return 0
}

# Restart the gnome shell if it is frozen.
restart_gnome_shell() {
  DISPLAY=:0 gnome-shell -r &
}

# Reset the permissions on /usr/local.
# Returns code 1 for failure, code 0 for success.
reset_usr_local() {
  sudo chown -R root:sudo /usr/local

  [[ ${?} -ne 0 ]] && return 1

  sudo chmod -R g+rw /usr/local

  [[ ${?} -ne 0 ]] && return 1

  return 0
}

# Set the golang path.
export GOPATH="${HOME}/go"

# Include golang applications in the global path.
export PATH="/usr/local/go/bin:${GOPATH}/bin:${PATH}"

# Set the text editor to either VIM or VI.
if [ "$(check_for_program 'vim')" == 'TRUE' ]; then
  export EDITOR=vim
else
  export EDITOR=vi
fi

# Set the command line editor to vi mode.
set -o vi

# Alias ll to a sorted long list.
alias ll='ls -ltr'

# Alias vim commands.
alias vim-c='vim -u ~/.vimrc.c'
alias vim-g='vim -u ~/.vimrc.g'
alias vim-r='vim -u ~/.vimrc.r'

# Source the cargo environment file for rust.
if [ -e "${HOME}/.cargo/env" ]; then
  source ${HOME}/.cargo/env
fi

# Set source path for rust (if rustc exists).
if [ "$(check_for_program 'rustc')" == 'TRUE' ]; then
  export RUST_SOURCE_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"
fi

# Force changing the directory to home.
cd ${HOME}
