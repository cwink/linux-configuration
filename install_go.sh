#!/bin/bash

# Set the path for GO.
export GOPATH="${HOME}/go"

# Source the bash utilities script.
source /usr/local/src/linux-configuration/utils.sh

if [ ${?} -ne 0 ]; then
  print_error 'Failed to source utilities script file.'

  exit 1
fi

# Check for required programs and exit if they don't exist.
PROGRAMS=( 'wget' 'curl' 'git' 'vim' )

for i in "${PROGRAMS[@]}"; do
  print_info "Checking if '${i}' is installed..."

  if [ "$(check_for_program ${i})" == 'FALSE' ]; then
    print_error "Failed to find program '${i}'."

    exit 2
  fi
done

# Check if writing to /usr/local is possible.
if [ "$(can_write_to_directory '/usr/local')" != 'TRUE' ]; then
  print_error 'Failed to write to directory /usr/local.'

  exit 3
fi

# Grabbing url for latest go distribution.
print_info 'Finding the latest go distribution url...'

GO_URL=`curl -k -L -s --connect-timeout 10 'https://golang.org/dl/' >&1 2>&1 | grep -E 'go.*linux.*tar.*gz' | grep 'download downloadBox' | cut -d'"' -f4-4`

GO_URL="https://golang.org${GO_URL}"

if [ "${GO_URL}" == '' ]; then
  print_error 'Failed to find the latest go distribution URL.'

  exit 4
fi

print_info "Downloading the latest go distribution from '${GO_URL}'..."

# Get the latest go distribution.
wget --timeout=10 -O 'go.tar.gz' "${GO_URL}" >/dev/null 1>/dev/null 2>&1

if [ ${?} -ne 0 ]; then
  print_error "Failed to download the latest go distribution from '${GO_URL}'."

  exit 5
fi

# Remove the existing go installation.
print_info 'Removing existing go installation...'

rm -rf '/usr/local/go'

rm -rf "${GOPATH}/bin"

# Extract the latest go distribution.
print_info 'Extracting the latest go distribution...'

tar -C '/usr/local' -xzf 'go.tar.gz' >/dev/null 1>/dev/null 2>&1

if [ ${?} -ne 0 ]; then
  print_error 'Failed to extract the latest go distribution.'

  exit 6
fi

# Remove the go archive.
rm -rf 'go.tar.gz' >/dev/null 1>/dev/null 2>&1

# Install vim plugins.
vim -u ~/.vimrc.g +PlugInstall +qall

# Install all go binaries for vim.
print_info 'Installing the latest go binaries for vim.'

export PATH="/usr/local/go/bin:${GOPATH}/bin:${PATH}"

vim -u ~/.vimrc.g +GoInstallBinaries +qall

if [ ${?} -ne 0 ]; then
  print_error 'Failed to install go binaries for vim.'

  exit 6
fi

exit 0
