call plug#begin('~/.vim/plugged')
  Plug 'tpope/vim-sensible'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'altercation/vim-colors-solarized'
  Plug 'scrooloose/nerdtree'
  Plug 'rust-lang/rust.vim'
  Plug 'valloric/youcompleteme'
  Plug 'preservim/tagbar'
call plug#end()

source ~/.vimrc.u

let g:rustfmt_autosave = 1

augroup RUST
  autocmd!
  autocmd BufEnter,BufCreate,BufRead *.rs setlocal filetype=rust
  autocmd FileType rust setlocal ts=4 sts=4 sw=4 expandtab
  autocmd FileType rust nnoremap <buffer><leader>b :Cbuild<cr>
  autocmd FileType rust nnoremap <buffer><leader>c :Cclean<cr>
  autocmd FileType rust nnoremap <buffer><leader>d :Cdoc<cr>
  autocmd FileType rust nnoremap <buffer><leader>r :Crun<cr>
  autocmd FileType rust nnoremap <buffer><leader>t :Ctest<cr>
  autocmd FileType rust nnoremap <buffer><leader>b :Cbench<cr>
  autocmd FileType rust nnoremap <buffer><leader>i :Cinstall<cr>
  autocmd FileType rust nnoremap <buffer><leader>u :Cupdate<cr>
  autocmd FileType rust nnoremap <buffer><leader>l :!cargo clippy --all-targets --all-features -- -D warnings -D clippy::all -D clippy::nursery -D clippy::pedantic -D clippy::cargo<cr>
  autocmd FileType rust nnoremap <buffer><leader>f :RustFmt<cr>
  autocmd FileType rust nnoremap <buffer><leader>g :TagbarToggle<cr>
augroup END
