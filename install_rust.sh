#!/bin/bash

# Ensure that curl and universal-ctags is installed.

sudo apt install curl universal-ctags

# Install rust.
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

# Install vim rust plugins.
vim -u ~/.vimrc.r +PlugInstall +qall

exit 0
