call plug#begin('~/.vim/plugged')
  Plug 'tpope/vim-sensible'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'altercation/vim-colors-solarized'
  Plug 'scrooloose/nerdtree'
  Plug 'valloric/youcompleteme'
  Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
call plug#end()

source ~/.vimrc.u

augroup GO
  autocmd!
  autocmd BufEnter,BufCreate,BufRead *.go setlocal filetype=go
  autocmd FileType go setlocal ts=8 sts=8 sw=8 noexpandtab
  autocmd FileType go nnoremap <buffer><leader>r :GoRun<cr>
  autocmd FileType go nnoremap <buffer><leader>t :GoTest<cr>
  autocmd FileType go nnoremap <buffer><leader>g :GoGenerate<cr>
  autocmd FileType go nnoremap <buffer><leader>l :GoLint<cr>
  autocmd FileType go nnoremap <buffer><leader>f :GoFmt<cr>
  autocmd FileType go nnoremap <buffer>gd :bd!<cr>
augroup END
