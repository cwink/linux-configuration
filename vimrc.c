call plug#begin('~/.vim/plugged')
  Plug 'tpope/vim-sensible'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'altercation/vim-colors-solarized'
  Plug 'scrooloose/nerdtree'
  Plug 'rhysd/vim-clang-format'
  Plug 'bfrg/vim-cpp-modern'
  Plug 'valloric/youcompleteme'
call plug#end()

source ~/.vimrc.u

"
" Set options for enhanched c++ syntax support.
"
let g:cpp_concepts_highlight = 1

"
" Turn off annoying confirmation from YCM.
"
let g:ycm_confirm_extra_conf = 0

"
" Set Termdebug settings.
"
packadd termdebug

let g:termdebug_popup = 0

let g:termdebug_wide = 163

let g:termdebug_map_K = 0

hi debugPC term=reverse ctermbg=black guibg=black

hi debugBreakpoint term=reverse ctermbg=darkmagenta guibg=darkmagenta

"
" Add extra functionality for C.
"
augroup C
  autocmd!
  autocmd BufEnter,BufCreate,BufRead *.c,*.h setlocal filetype=c
  autocmd FileType c nnoremap <buffer><leader>cm :!cd build && CC=clang cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && cd -<cr>
  autocmd FileType c nnoremap <buffer><leader>cr :!cd build && make && make run && cd -<cr>
  autocmd FileType c nnoremap <buffer><leader>cc :!cd build && make clean && cd -<cr>
  autocmd FileType c nnoremap <buffer><leader>cf :!cd build && make format && cd -<cr>
  autocmd FileType c nnoremap <buffer><leader>ct :!cd build && make && make tests && cd -<cr>
  autocmd FileType c nnoremap <buffer><leader>f :ClangFormat<cr>
  autocmd FileType c nnoremap <buffer><leader>yd :YcmCompleter GetDoc<cr>
  autocmd FileType c nnoremap <buffer><leader>yg :YcmCompleter GoTo<cr>
  autocmd FileType c nnoremap <buffer><leader>dd :Termdebug 
  autocmd FileType c nnoremap <buffer><leader>dr :Run<cr>
  autocmd FileType c nnoremap <buffer><leader>db :Break<cr>
  autocmd FileType c nnoremap <buffer><leader>dB :Clear<cr>
  autocmd FileType c nnoremap <buffer><leader>ds :Step<cr>
  autocmd FileType c nnoremap <buffer><leader>dn :Over<cr>
  autocmd FileType c nnoremap <buffer><leader>dc :Continue<cr>
  autocmd FileType c nnoremap <buffer><leader>df :Finish<cr>
  autocmd FileType c nnoremap <buffer><leader>dq :call TermDebugSendCommand('set confirm off')<cr>:call TermDebugSendCommand('quit')<cr>
  autocmd FileType c nnoremap <buffer><leader>de :Evaluate<cr>
augroup END

"
" Add extra functionality for C++.
"
augroup CPP
  autocmd!
  autocmd BufEnter,BufCreate,BufRead *.cpp,*.hpp,*.impl,*.tpp setlocal filetype=cpp
  autocmd FileType cpp nnoremap <buffer><leader>cm :!cd build && CXX=clang++ CC=clang cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && cd -<cr>
  autocmd FileType cpp nnoremap <buffer><leader>cr :!cd build && make && make run && cd -<cr>
  autocmd FileType cpp nnoremap <buffer><leader>cc :!cd build && make clean && cd -<cr>
  autocmd FileType cpp nnoremap <buffer><leader>ct :!cd build && make tidy && cd -<cr>
  autocmd FileType cpp nnoremap <buffer><leader>cf :!cd build && make format && cd -<cr>
  autocmd FileType cpp nnoremap <buffer><leader>cs :!cd build && make tests && cd -<cr>
  autocmd FileType cpp nnoremap <buffer><leader>f :ClangFormat<cr>
  autocmd FileType cpp nnoremap <buffer><leader>yd :YcmCompleter GetDoc<cr>
  autocmd FileType cpp nnoremap <buffer><leader>yg :YcmCompleter GoTo<cr>
  autocmd FileType c nnoremap <buffer><leader>dd :Termdebug 
  autocmd FileType c nnoremap <buffer><leader>dr :Run<cr>
  autocmd FileType c nnoremap <buffer><leader>db :Break<cr>
  autocmd FileType c nnoremap <buffer><leader>dB :Clear<cr>
  autocmd FileType c nnoremap <buffer><leader>ds :Step<cr>
  autocmd FileType c nnoremap <buffer><leader>dn :Over<cr>
  autocmd FileType c nnoremap <buffer><leader>dc :Continue<cr>
  autocmd FileType c nnoremap <buffer><leader>df :Finish<cr>
  autocmd FileType c nnoremap <buffer><leader>dq :call TermDebugSendCommand('set confirm off')<cr>:call TermDebugSendCommand('quit')<cr>
  autocmd FileType c nnoremap <buffer><leader>de :Evaluate<cr>
augroup END
