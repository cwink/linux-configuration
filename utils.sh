#!/bin/bash

# Provide the path to the linux-configuration folder.
export CONFIG_FOLDER='/usr/local/src/linux-configuration'

# Function to trim a string.
#   1 - The string to trim.
# Returns the trimmed string.
trim_string() {
  local STRING="${1}"

  echo "${STRING}" | tr -d '[:cntrl:]' | sed -e 's/^\s*//g' -e 's/\s*$//g' -e 's/^\t*//g' -e 's/\t*$//g'

  return 0
}

# Function to determine if a program exists or not.
#   1 - The program to check for.
# Returns TRUE if it exists, FALSE otherwise.
check_for_program() {
  local PROGRAM=`trim_string "${1}"`

  which ${PROGRAM} >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    echo 'FALSE'
  else
    echo 'TRUE'
  fi

  return 0
}

# Cleans a string of non-alphanumeric characters.
# Return the clean string.
clean_string() {
  local STRING="${1}"

  printf "${STRING}" | tr -dc '[:alnum:][:space:][:punct:]\n\r'

  return 0
}

# Sanatize a string by cleaning it and then trimming it.
# Return the sanatized string.
sanatize_string() {
  local STRING="${1}"

  local CLEANED_STRING=`clean_string "${STRING}"`

  trim_string "${CLEANED_STRING}"

  return 0
}

# Check if a file contains the given regular expression.
#   1 - The file to check.
#   2 - The regular expression to check for.
# Return TRUE if the file contains the regular expression, FALSE otherwise.
file_contains_regex() {
  local FILE=`trim_string "${1}"`

  local REGEX=`trim_string "${2}"`

  local HAS=`cat "${FILE}" | grep -E "${REGEX}"`

  if [ "${HAS}" != '' ]; then
    echo 'TRUE'
  else
    echo 'FALSE'
  fi

  return 0
}

# Check if able to write to the given directory.
#   1 - The directory to check if it's writable.
# Returns TRUE if it is writable, FALSE otherwise.
can_write_to_directory() {
  local DIRECTORY=`trim_string "${1}"`

  local FILE="${DIRECTORY}/$(uuidgen).tmp"

  touch "${FILE}" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    echo 'FALSE'
  else
    echo 'TRUE'
  fi

  rm -rf "${FILE}"

  return 0
}

# Print an information message to stdout.
print_info() {
  local MESSAGE="${1}"

  echo "INFO: ${MESSAGE}" >&1 2>&1
}

# Print an error message to stderr.
print_error() {
  local MESSAGE="${1}"

  echo "ERROR: ${MESSAGE}" >&2 1>&2
}
