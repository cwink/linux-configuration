#!/bin/bash

# Source the bash utilities script.
source /usr/local/src/linux-configuration/utils.sh

if [ ${?} -ne 0 ]; then
  print_error 'Failed to source utilities script file.'

  exit 1
fi

# Check for required programs and exit if they don't exist.
PROGRAMS=( 'curl' 'git' 'vim' )

for i in "${PROGRAMS[@]}"; do
  print_info "Checking if '${i}' is installed..."

  if [ "$(check_for_program ${i})" == 'FALSE' ]; then
    print_error "Failed to find program '${i}'."

    exit 2
  fi
done

# Create the .vim home directory if it doesn't exist.
print_info "Creating '${HOME}/.vim' folder..."

mkdir -p "${HOME}/.vim"

# If the home bashrc file doesn't source the linux-configuration bashrc, add a sourcing line to the home bashrc.
if [ `file_contains_regex "${HOME}/.bashrc" "(source|[.]) '${CONFIG_FOLDER}/bashrc'"` == 'FALSE' ]; then
  print_info "Adding sourcing line for bashrc to '${HOME}/.bashrc'..."

  echo "source '${CONFIG_FOLDER}/bashrc'" >> "${HOME}/.bashrc"
fi

# If the home vimrc file doesn't exist, create a link to the linux-configuration vimrc file.
print_info 'Creating link between home vimrc and linux-configuration vimrc...'

if [ ! -f "${HOME}/.vimrc" ]; then
  ln -s "${CONFIG_FOLDER}/vimrc" "${HOME}/.vimrc" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error "Unable to create a link between '${CONFIG_FOLDER}' and '${HOME}/.vimrc'."

    exit 3
  fi
fi

if [ ! -f "${HOME}/.vimrc.u" ]; then
  ln -s "${CONFIG_FOLDER}/vimrc.u" "${HOME}/.vimrc.u" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error "Unable to create a link between '${CONFIG_FOLDER}' and '${HOME}/.vimrc.u'."

    exit 3
  fi
fi

if [ ! -f "${HOME}/.vimrc.c" ]; then
  ln -s "${CONFIG_FOLDER}/vimrc.c" "${HOME}/.vimrc.c" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error "Unable to create a link between '${CONFIG_FOLDER}' and '${HOME}/.vimrc.c'."

    exit 3
  fi
fi

if [ ! -f "${HOME}/.vimrc.g" ]; then
  ln -s "${CONFIG_FOLDER}/vimrc.g" "${HOME}/.vimrc.g" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error "Unable to create a link between '${CONFIG_FOLDER}' and '${HOME}/.vimrc.g'."

    exit 3
  fi
fi

if [ ! -f "${HOME}/.vimrc.r" ]; then
  ln -s "${CONFIG_FOLDER}/vimrc.r" "${HOME}/.vimrc.r" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error "Unable to create a link between '${CONFIG_FOLDER}' and '${HOME}/.vimrc.r'."

    exit 3
  fi
fi

# If vim-plug hasn't been installed yet, install it.
print_info 'Installing vim-plug...'

if [ ! -f "${HOME}/.vim/autoload/plug.vim" ]; then
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error 'Unable to install vim-plug.'

    exit 4
  fi
fi

# Install the powerline fonts collection.
print_info 'Installing powerline fonts collection...'

ORIGINAL_PATH=`pwd`

cd "${CONFIG_FOLDER}" >/dev/null 2>&1

if [ ! -e 'fonts' ]; then
  git clone https://github.com/powerline/fonts.git --depth=1 >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error 'Failed to install the powerline-fonts collection.'

    exit 5
  fi
fi

cd fonts >/dev/null 2>&1

chmod +x install.sh >/dev/null 2>&1

./install.sh >/dev/null 2>&1

cd "${ORIGINAL_PATH}" >/dev/null 2>&1

# Install all vim plugins automatically.
print_info 'Installing vim plugins...'

vim +PlugInstall +qall

if [ ${?} -ne 0 ]; then
  print_error 'Failed to install vim plugins.'

  exit 6
fi

# Remove old viminfo file.
print_info "Removing old '${HOME}/.viminfo'..."

rm -rf "${HOME}/.viminfo"

# Install solarized dircolors.
print_info 'Installing solarized dircolors...'

cd "${CONFIG_FOLDER}" >/dev/null 2>&1

if [ ! -e 'dircolors-solarized' ]; then
  git clone https://github.com/seebi/dircolors-solarized.git >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error 'Failed to install dircolors solarized theme.'

    exit 7
  fi
fi

if [ ! -e "${HOME}/.dircolors" ]; then
  ln -s "${CONFIG_FOLDER}/dircolors-solarized/dircolors.ansi-dark" "${HOME}/.dircolors" >/dev/null 2>&1

  if [ ${?} -ne 0 ]; then
    print_error 'Failed to link dircolors solarized theme.'

    exit 8
  fi
fi

# Install MinTTY Solarized Color Scheme if using MinTTY
if [ "${TERM_PROGRAM}" == 'mintty' ]; then
  print_info 'Installing MinTTY solarized colorscheme...'

  git clone https://github.com/mavnn/mintty-colors-solarized.git >/dev/null 2>&1

  cat mintty-colors-solarized/.minttyrc.dark >>${HOME}/.minttyrc
fi

# Setup and install YouCompleteMe.
print_info 'Installing YCM dependencies...'

OPERATING_SYSTEM=`hostnamectl | grep 'Operating System' | cut -d':' -f2 | awk '{ print $1 }'`

case "${OPERATING_SYSTEM}" in
  'Ubuntu')
    sudo apt install -y build-essential cmake python3-dev >/dev/null 2>&1

    ;;
  'Fedora')
    sudo dnf install cmake gcc-c++ make python3-devel >/dev/null 2>&1

    ;;
  '*')
    print_error "Couldn't install YCM dependencies, please install manually."

    exit 9
    ;;
esac

cd "${HOME}/.vim/plugged/youcompleteme"

python3 install.py --clangd-completer --go-completer --rust-completer >/dev/null 2>&1

cd - >/dev/null 2>&1

exit 0
