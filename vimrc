"
" The following must be run before using this script:
"
"   curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" Also, 'git' will need to be installed for vim-plug to work.
"
" Power Fonts are also used for Airline, get them via:
"
"   git clone https://github.com/powerline/fonts.git --depth=1
"   cd fonts
"   ./install.sh
"
" To install binaries for golang support, run the following:
"   :GoInstallBinaries
"

call plug#begin('~/.vim/plugged')
  Plug 'tpope/vim-sensible'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'altercation/vim-colors-solarized'
  Plug 'scrooloose/nerdtree'
call plug#end()

source ~/.vimrc.u
